import yaml
import graphviz
import argparse
import re


def parse_yaml(file_path):
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)
    return data


def extract_connections(data):
    connections = []
    for connection_group in data['connections']:
        if len(connection_group) < 3:
            continue
        for i in range(1, len(connection_group) - 1):
            source = connection_group[0]
            cable = connection_group[i]
            destination = connection_group[i + 1]
            connections.append((source, cable, destination))
    return connections


def group_connectors_by_system(connections):
    systems = {}
    pattern = re.compile(r'(.*)\s(.*)$')

    for source, _, destination in connections:
        src_match = pattern.match((*source,)[0])
        dst_match = pattern.match((*destination,)[0])

        if src_match and dst_match:
            src_system, src_connector = src_match.groups()
            dst_system, dst_connector = dst_match.groups()

            if src_system not in systems:
                systems[src_system] = []
            if dst_system not in systems:
                systems[dst_system] = []

            if src_connector not in systems[src_system]:
                systems[src_system].append(src_connector)
            if dst_connector not in systems[dst_system]:
                systems[dst_system].append(dst_connector)

    return systems


def create_block_diagram(systems, connections):
    dot = graphviz.Digraph(
        comment='System Level Connection Diagram', engine='dot')
    dot.attr(splines='ortho')
    dot.attr(overlap='vpsc')
    pattern = re.compile(r'(.*)\s(.*)$')

    # Add system blocks
    idx = 1
    for system, connectors in systems.items():
        with dot.subgraph(name='cluster_' + system) as sub:
            sub.attr(label=system, bgcolor=f'/set312/{idx}')
            idx += 1
            for connector in connectors:
                sub.node(f"{system} {connector}",
                         label=connector, shape='rectangle', style='filled',
                         color='black', fillcolor='white',
                         fixedsize='True', width='1.2', height='0.4')
    # Add connections between connectors
    for source, cable, destination in connections:
        src_match = pattern.match((*source,)[0])
        dst_match = pattern.match((*destination,)[0])
        cable = (*cable,)[0]

        if src_match and dst_match:
            src_system, src_connector = src_match.groups()
            dst_system, dst_connector = dst_match.groups()
            print(src_system, src_connector)
            print(dst_system, dst_connector, cable)
            dot.edge(f"{src_system} {src_connector}",
                     f"{dst_system} {dst_connector}",
                     label=cable, dir='none', constraint='true')

    return dot


def main():
    parser = argparse.ArgumentParser(
        description='Generate a system-level connection diagram from a WireViz YAML file.')
    parser.add_argument(
        'file', type=str, help='The path to the WireViz YAML file.')
    parser.add_argument('-o',
                        '--output',
                        dest='outfile',
                        type=str, help='Output file name. If not set [source_file].png is used')
    args = parser.parse_args()

    file_path = args.file
    data = parse_yaml(file_path)
    connections = extract_connections(data)
    systems = group_connectors_by_system(connections)
    # print(systems)
    dot = create_block_diagram(systems, connections)

    output_file = args.outfile or file_path.replace('.yaml', '')
    dot.render(output_file, format='png', directory='output', cleanup=True)
    dot.render(output_file, format='svg', directory='output', cleanup=True)
    print(f"Diagram saved as {output_file}.png")


if __name__ == '__main__':
    main()
