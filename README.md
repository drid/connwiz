 
# ConnWiz

ConnWiz is a tool designed to create system-level connection diagrams from Wireviz YAML files. Instead of generating detailed graphs for each cable and connection, ConnWiz provides a high-level block diagram that groups connectors by system and shows inter-system connections clearly.

## Description

ConnWiz helps visualize complex wiring and connector setups by converting Wireviz YAML files into simplified block diagrams. This is especially useful for engineers and technicians who need an overview of system connections without diving into the minutiae of each individual cable.

## Features

- Parses Wireviz YAML files to extract connection information.
- Generates block diagrams with systems and their connectors.
- Displays connections between systems with labeled edges.
- Ensures clear, non-overlapping connection lines.

## Installation

1. Ensure you have Python installed.
2. Install the required Python packages using pip:
   ```sh
   pip install graphviz pyyaml
   ```

## Usage

1. Prepare your Wireviz YAML file with the connection details.
2. Use the ConnWiz script to generate the diagram.

### Command-Line Usage

Run the script from the command line with the YAML file as an argument:

```sh
python connwiz.py -f path/to/your_file.yaml [-o output_file.png]
```

### Options

- `-f/--file`: Path to the input Wireviz YAML file.
- `-o/--output`: (Optional) Path to the output image file. If not provided, the output file will be named after the input file with a `.png` extension.

### Example

Given a Wireviz YAML file (`connections.yaml`):

```yaml
connections:
  -
    - BAT J12: [1-2]
    - BAT-RBF: [1-2]
    - Umbilical J7: [1-2]
  -
    - SA-Y+ J14: [1-4]
    - PV Deploy: [1-4]
    - Umbilical J9: [1-4]
```

Running ConnWiz:

```sh
python connwiz.py -f connections.yaml -o diagram.png
```

This command will generate a block diagram in PNG format, saved as `diagram.png`, displaying the connections defined in `connections.yaml`.

If the `-o` option is not provided:

```sh
python connwiz.py -f connections.yaml
```
![SamleImage](connections.png)

The output file will be named `connections.png`.

## Naming Scheme for Systems and Connections

### Systems

Systems are identified based on the prefixes of connector names. For example, in `EPS J1`:
- `EPS` is the system name.
- `J1` is the connector name.

The script assumes connectors follow a pattern where the connector name starts after the last space of the cable name (e.g., `J1`, `J01`, `TST`). Connectors are grouped into systems based on the prefix before the connector name.

### Connections

Connections between systems are defined in the YAML file. Each connection block specifies which connectors are linked, with cables between them. For example:

```yaml
connections:
  -
    - BAT J12: [1-2]
    - BAT-RBF: [1-2]
    - Umbilical J7: [1-2]
```

This defines a connection between `BAT J12` and `Umbilical J7` using the specified cable named BAT-RBF.

## Contributing

Contributions are welcome! Feel free to open issues or submit pull requests on GitLub.

## License

This project is licensed under the GPLv3 License. See the [LICENSE](LICENSE) file for details.

## Acknowledgements

- [Wireviz](https://github.com/plotprojects/wireviz) for providing a great tool to define connections in YAML.
- [Graphviz](https://graphviz.org/) for the graph visualization capabilities.


